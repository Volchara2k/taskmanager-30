package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@SuppressWarnings("unused")
public interface ITaskEndpoint {

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    TaskDTO addTaskForProject(
            @Nullable SessionDTO sessionDTO,
            @Nullable String projectTitle,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    TaskDTO addTask(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    TaskDTO updateTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    TaskDTO updateTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    TaskDTO deleteTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    TaskDTO deleteTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    TaskDTO deleteTaskByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @WebMethod
    @WebResult(name = "deletedTasks", partName = "deletedTasks")
    @NotNull
    Collection<TaskDTO> deleteAllTasks(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    TaskDTO getTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    TaskDTO getTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    TaskDTO getTaskByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    Collection<TaskDTO> getAllTasks(
            @Nullable SessionDTO sessionDTO
    );

}