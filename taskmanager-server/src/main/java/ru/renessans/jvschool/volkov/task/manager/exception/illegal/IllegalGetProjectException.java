package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalGetProjectException extends AbstractException {

    @NotNull
    private static final String MODEL_ADD_ILLEGAL =
            "Ошибка! Выполнение получения модели завершилось нелегальным образом!\n" +
                    "Убедитесь, что вы ввели правильный заголовок проекта!";

    public IllegalGetProjectException() {
        super(MODEL_ADD_ILLEGAL);
    }

}