package ru.renessans.jvschool.volkov.task.manager.exception.invalid.file;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidFileException extends AbstractException {

    @NotNull
    private static final String EMPTY_FILE =
            "Ошибка! Параметр \"файл\" является пустым, null, или не существует в системе!\n";

    public InvalidFileException() {
        super(EMPTY_FILE);
    }

}