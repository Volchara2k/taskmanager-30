package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

public final class ProjectUserService extends AbstractUserOwnerService<Project, IProjectUserRepository> implements IProjectUserService {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    public ProjectUserService(
            @NotNull final IEntityManagerFactoryService managerFactoryService
    ) {
        this.managerFactoryService = managerFactoryService;
    }

    @NotNull
    @Override
    protected IProjectUserRepository createRepository() {
        return new ProjectUserRepository(this.managerFactoryService.getEntityManager());
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Project project = new Project(userId, title, description);
        return super.beginTransactionForResult(repository -> repository.persist(project));
    }

}