package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;

@AllArgsConstructor
public final class ServiceContextService implements IServiceContextService {

    private final IServiceContextRepository serviceLocatorRepository;

    @NotNull
    @Override
    public IUserService getUserService() {
        return this.serviceLocatorRepository.getUserService();
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return this.serviceLocatorRepository.getSessionService();
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.serviceLocatorRepository.getAuthenticationService();
    }

    @NotNull
    @Override
    public ITaskUserService getTaskService() {
        return this.serviceLocatorRepository.getTaskService();
    }

    @NotNull
    @Override
    public IProjectUserService getProjectService() {
        return this.serviceLocatorRepository.getProjectService();
    }

    @NotNull
    @Override
    public IDataInterChangeService getDataInterChangeService() {
        return this.serviceLocatorRepository.getDataInterChangeService();
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return this.serviceLocatorRepository.getPropertyService();
    }

    @NotNull
    @Override
    public IConfigurationService getConfigurationService() {
        return this.serviceLocatorRepository.getConfigurationService();
    }

    @NotNull
    @Override
    public IEntityManagerFactoryService getEntityManagerService() {
        return this.serviceLocatorRepository.getEntityManagerService();
    }

    @NotNull
    @Override
    public IAdapterContextService getAdapterService() {
        return this.serviceLocatorRepository.getAdapterService();
    }

}