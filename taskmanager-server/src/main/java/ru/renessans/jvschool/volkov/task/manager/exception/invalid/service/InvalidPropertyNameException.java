package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPropertyNameException extends AbstractException {

    @NotNull
    private static final String EMPTY_NAME = "Ошибка! Параметр \"название свойств\" является пустым или null!\n";

    public InvalidPropertyNameException() {
        super(EMPTY_NAME);
    }

}