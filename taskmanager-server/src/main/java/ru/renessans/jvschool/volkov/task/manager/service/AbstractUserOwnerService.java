package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.*;

public abstract class AbstractUserOwnerService<E extends AbstractUserOwner, R extends IOwnerUserRepository<E>> extends AbstractService<E, R> implements IOwnerUserService<E, R> {

    @NotNull
    @SneakyThrows
    @Override
    public E updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = getByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.merge(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = getById(userId, id);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.merge(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return super.beginTransactionForResult(repository -> repository.deleteById(userId, id));
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return super.beginTransactionForResult(repository -> repository.deleteByTitle(userId, title));
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> deleteAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return super.beginTransactionForResult(repository -> repository.deleteAll(userId));
    }

    @NotNull
    @SneakyThrows
    @Override
    public E deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return super.beginTransactionForResult(repository -> repository.deleteByIndex(userId, index));
    }

    @Nullable
    @SneakyThrows
    @Override
    public E getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return super.beginTransactionForResult(repository -> repository.getByIndex(userId, index));
    }

    @Nullable
    @SneakyThrows
    @Override
    public E getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return super.beginTransactionForResult(repository -> repository.getById(userId, id));
    }

    @Nullable
    @SneakyThrows
    @Override
    public E getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return super.beginTransactionForResult(repository -> repository.getByTitle(userId, title));
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> getAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return super.beginTransactionForResult(repository -> repository.getAll(userId));
    }

    @NotNull
    @Override
    public Collection<E> getAllRecords() {
        return super.beginTransactionForResult(IOwnerUserRepository::getAllRecords);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialDemoData(
            @Nullable final Collection<User> users
    ) {
        if (ValidRuleUtil.isNullOrEmpty(users)) throw new InvalidUserException();

        @NotNull final List<E> demoData = new ArrayList<>();
        users.forEach(user -> {
            deleteAll(user.getId());
            @NotNull final E task = add(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
            demoData.add(task);
        });

        return demoData;
    }

}