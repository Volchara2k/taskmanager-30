package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.*;

public final class AdapterContextRepository implements IAdapterContextRepository {

    @NotNull
    private final IProjectAdapterService projectAdapter = new ProjectAdapterService();

    @NotNull
    private final ISessionAdapterService sessionAdapter = new SessionAdapterService();

    @NotNull
    private final ITaskAdapterService taskAdapter = new TaskAdapterService();

    @NotNull
    private final IUserLimitedAdapterService userLimitedAdapter = new UserLimitedAdapterService();

    @NotNull
    private final IUserUnlimitedAdapterService userUnlimitedAdapter = new UserUnlimitedAdapterService();

    @NotNull
    @Override
    public IProjectAdapterService getProjectAdapter() {
        return this.projectAdapter;
    }

    @NotNull
    @Override
    public ISessionAdapterService getSessionAdapter() {
        return this.sessionAdapter;
    }

    @NotNull
    @Override
    public ITaskAdapterService getTaskAdapter() {
        return this.taskAdapter;
    }

    @NotNull
    @Override
    public IUserLimitedAdapterService getUserLimitedAdapter() {
        return this.userLimitedAdapter;
    }

    @NotNull
    @Override
    public IUserUnlimitedAdapterService getUserUnlimitedAdapter() {
        return this.userUnlimitedAdapter;
    }

}