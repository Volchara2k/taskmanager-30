package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalTransactionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValuesException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService<E extends AbstractModel, R extends IRepository<E>> implements IService<E, R> {

    @NotNull
    protected abstract R createRepository();

    @NotNull
    @SneakyThrows
    @Override
    public E persist(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return beginTransactionForResult(repository -> repository.persist(value));
    }

    @NotNull
    @SneakyThrows
    @Override
    public E merge(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return beginTransactionForResult(repository -> repository.merge(value));
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> setAllRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidValuesException();
        return beginTransactionForResult(repository -> repository.setAllRecords(values));
    }

    @Nullable
    @SneakyThrows
    @Override
    public E deletedRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return beginTransactionForResult(repository -> repository.deleteRecord(value));
    }

    @SneakyThrows
    @Override
    public boolean deletedAllRecords() {
        return beginTransactionForResult(IRepository::deleteAllRecords);
    }

    @SneakyThrows
    public <T> T beginTransactionForResult(
            @NotNull final Function<R, T> lambda
    ) {
        @NotNull final R repository = createRepository();
        @Nullable final T result;

        try {
            repository.beginTransaction();
            result = lambda.apply(repository);
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalTransactionException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

}