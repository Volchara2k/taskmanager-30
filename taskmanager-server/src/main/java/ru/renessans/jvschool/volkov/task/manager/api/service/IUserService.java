package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

public interface IUserService extends IService<User, IUserRepository> {

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

    @NotNull
    User addUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRole userRole
    );

    @Nullable
    User getUserById(
            @Nullable String id
    );

    @Nullable
    User getUserByLogin(
            @Nullable String login
    );

    @NotNull
    UserRole getUserRole(
            @Nullable String userId
    );

    @Nullable
    User editProfileById(
            @Nullable String id,
            @Nullable String firstName
    );

    @Nullable
    User editProfileById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    User updatePasswordById(
            @Nullable String id,
            @Nullable String newPassword
    );

    @Nullable
    User lockUserByLogin(
            @Nullable String login
    );

    @Nullable
    User unlockUserByLogin(
            @Nullable String login
    );

    @NotNull
    User deleteUserById(
            @Nullable String id
    );

    @NotNull
    User deleteUserByLogin(
            @Nullable String login
    );

    @NotNull
    Collection<User> getAllRecords();

    @NotNull
    Collection<User> initialDemoData();

}