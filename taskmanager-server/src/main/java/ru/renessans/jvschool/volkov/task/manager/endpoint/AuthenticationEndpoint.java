package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserLimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
public final class AuthenticationEndpoint extends AbstractEndpoint implements IAuthenticationEndpoint {

    public AuthenticationEndpoint() {
    }

    public AuthenticationEndpoint(
            @NotNull final IServiceContextService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = authService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());

        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @NotNull final User user = authService.signUp(login, password);
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUserWithFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = authService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());

        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @NotNull final User user = authService.signUp(login, password, firstName);
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

}