package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class ProjectUserRepository extends AbstractUserOwnerRepository<Project> implements IProjectUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectUserRepository(
            @NotNull final EntityManager entityManager
    ) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Collection<Project> getAll(
            @NotNull final String userId
    ) {
        @NotNull final String jpqlQuery = "FROM Project WHERE user.id = :userId ORDER BY creationDate";
        return this.entityManager.createQuery(jpqlQuery, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final String jpqlQuery = "FROM Project WHERE user.id = :userId ORDER BY creationDate";
        @NotNull final List<Project> projects = this.entityManager.createQuery(jpqlQuery, Project.class)
                .setParameter("userId", userId)
                .getResultList();
        if (projects.size() < index) return null;
        return projects.get(index);
    }

    @Nullable
    @Override
    public Project getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpqlQuery = "FROM Project WHERE user.id = :userId AND id = :id";
        @NotNull final List<Project> projects = this.entityManager.createQuery(jpqlQuery, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @NotNull final String jpqlQuery = "FROM Project WHERE user.id = :userId AND title = :title";
        @NotNull final List<Project> projects = this.entityManager.createQuery(jpqlQuery, Project.class)
                .setParameter("userId", userId)
                .setParameter("title", title)
                .setMaxResults(1)
                .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @NotNull
    @Override
    public Collection<Project> getAllRecords() {
        @NotNull final String jpqlQuery = "FROM Project ORDER BY creationDate";
        return this.entityManager.createQuery(jpqlQuery, Project.class)
                .getResultList();
    }

}