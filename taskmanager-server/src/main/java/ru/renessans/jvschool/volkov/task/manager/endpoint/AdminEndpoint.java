package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.ProjectAdapterService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    public AdminEndpoint() {
    }

    public AdminEndpoint(
            @NotNull final IServiceContextService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "serverData", partName = "serverData")
    @Override
    public String serverData(
            @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session open = sessionService.validateSession(conversion);
        sessionService.validateSession(open, ROLE);

        @NotNull final IConfigurationService configService = super.serviceLocator.getConfigurationService();
        return String.format(
                "Хост: %s\nпорт: %d",
                configService.getServerHost(), configService.getServerPort()
        );
    }

    @WebMethod
    @WebResult(name = "closedAllSessions", partName = "closedAllSessions")
    @Override
    public boolean closedAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session open = sessionService.validateSession(conversion);
        sessionService.validateSession(open, ROLE);
        return sessionService.closeAllSessions(open);
    }

    @WebMethod
    @WebResult(name = "sessions", partName = "sessions")
    @NotNull
    @Override
    public Collection<SessionDTO> getAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        return sessionService.getAllRecords()
                .stream()
                .map(sessionAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO signUpUserWithUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "userRole", partName = "userRole") @Nullable final UserRole role
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @NotNull final User user = authService.signUp(login, password, role);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    @Override
    public UserLimitedDTO deleteUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.deleteUserById(id);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    @Override
    public UserLimitedDTO deleteUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session open = sessionService.validateSession(conversion);
        sessionService.validateSession(open, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.deleteUserByLogin(login);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "deletedUsers", partName = "deletedUsers")
    @Override
    public boolean deleteAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session open = sessionService.validateSession(conversion);
        sessionService.validateSession(open, ROLE);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();

        return userService.deletedAllRecords();
    }

    @WebMethod
    @WebResult(name = "lockedUser", partName = "lockedUser")
    @Nullable
    @Override
    public UserLimitedDTO lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.lockUserByLogin(login);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "unlockedUser", partName = "unlockedUser")
    @Nullable
    @Override
    public UserLimitedDTO unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.unlockUserByLogin(login);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @Nullable
    @Override
    public Collection<UserLimitedDTO> setAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "users", partName = "users") @Nullable final Collection<UserLimitedDTO> usersDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        userService.setAllRecords(usersDTO != null ? usersDTO
                .stream()
                .map(userAdapter::toModel)
                .collect(Collectors.toList()) : null);
        return usersDTO;
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    @Override
    public Collection<UserLimitedDTO> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        return userService.getAllRecords()
                .stream()
                .map(userAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        return taskService.getAllRecords()
                .stream()
                .map(taskAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @Nullable
    @Override
    public Collection<TaskDTO> setAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "tasks", partName = "tasks") @Nullable final Collection<TaskDTO> tasksDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        taskService.setAllRecords(tasksDTO != null ? tasksDTO
                .stream()
                .map(taskAdapter::toModel)
                .collect(Collectors.toList()) : null);
        return tasksDTO;
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @NotNull final IProjectAdapterService projectAdapter = adapterService.getProjectAdapter();
        return projectService.getAllRecords()
                .stream()
                .map(projectAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> setAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projects", partName = "projects") @Nullable final Collection<ProjectDTO> projectsDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @NotNull final IAdapterService<ProjectDTO, Project> projectAdapter = new ProjectAdapterService();
        projectService.setAllRecords(Objects.requireNonNull(projectsDTO)
                .stream()
                .map(projectAdapter::toModel)
                .collect(Collectors.toList()));
        return projectsDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.getUserById(id);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.getUserByLogin(login);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfileById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.editProfileById(id, firstName);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfileByIdWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.editProfileById(id, firstName, lastName);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @Nullable
    @Override
    public UserLimitedDTO updatePasswordById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterContextService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.updatePasswordById(id, newPassword);

        @NotNull final IUserLimitedAdapterService userAdapter = adapterService.getUserLimitedAdapter();
        @Nullable final UserLimitedDTO userLimitedDTO = userAdapter.toDTO(user);
        return userLimitedDTO;
    }

}