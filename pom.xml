<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.renessans.jvschool.volkov.task.manager</groupId>
    <artifactId>taskmanager</artifactId>
    <packaging>pom</packaging>
    <version>1.0.30</version>

    <name>Task Manager</name>

    <developers>
        <developer>
            <id>Volkov</id>
            <name>Valery Volkov</name>
            <email>volkov.valery2013@yandex.ru</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <lombock.version>1.18.16</lombock.version>
        <jetbrains.annotations.version>20.1.0</jetbrains.annotations.version>
        <junit.version>4.12</junit.version>
        <junit.params.version>1.1.0</junit.params.version>
        <junit.jupiter.version>5.7.0</junit.jupiter.version>
        <enunciate.plugin.version>2.12.1</enunciate.plugin.version>
        <maven.compiler.plugin.version>2.5.1</maven.compiler.plugin.version>
        <maven.surefire.plugin>2.21.0</maven.surefire.plugin>
        <maven.dependency.plugin.version>3.1.2</maven.dependency.plugin.version>
        <maven.resources.plugin.version>3.0.2</maven.resources.plugin.version>
        <cobertura.plugin.version>2.7</cobertura.plugin.version>
        <path.category>ru.renessans.jvschool.volkov.task.manager.marker</path.category>
    </properties>

    <modules>
        <module>taskmanager-server</module>
        <module>taskmanager-client</module>
    </modules>

    <dependencies>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombock.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>${jetbrains.annotations.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>pl.pragmatists</groupId>
            <artifactId>JUnitParams</artifactId>
            <version>${junit.params.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>

        <profile>
            <id>Integration-tests-run</id>

            <properties>
                <test.category>
                    ${path.category}.IntegrationImplementation
                </test.category>
            </properties>
        </profile>

        <profile>
            <id>Negative-and-positive-tests-run</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <test.category>
                    ${path.category}.PositiveImplementation,
                    ${path.category}.NegativeImplementation
                </test.category>
            </properties>
        </profile>

        <profile>
            <id>Negative-tests-run</id>
            <properties>
                <test.category>${path.category}.NegativeImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Positive-tests-run</id>
            <properties>
                <test.category>${path.category}.PositiveImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Repository-tests-run</id>
            <properties>
                <test.category>${path.category}.RepositoryImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Service-tests-run</id>
            <properties>
                <test.category>${path.category}.ServiceImplementation</test.category>
            </properties>
        </profile>

        <profile>
            <id>Utility-tests-run</id>
            <properties>
                <test.category>${path.category}.UtilityImplementation</test.category>
            </properties>
        </profile>

    </profiles>

    <build>
        <plugins>

            <plugin>
                <groupId>com.webcohesion.enunciate</groupId>
                <artifactId>enunciate-maven-plugin</artifactId>
                <version>${enunciate.plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>docs</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <docsDir>${project.build.directory}/docs</docsDir>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.surefire.plugin}</version>
                <configuration>
                    <groups>${test.category}</groups>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>${cobertura.plugin.version}</version>
            </plugin>

        </plugins>

    </build>

</project>