package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;

public interface IApplicationContextService {

    @NotNull
    IEndpointContextService getEndpointContext();

    @NotNull
    IServiceContextService getServiceContext();

}