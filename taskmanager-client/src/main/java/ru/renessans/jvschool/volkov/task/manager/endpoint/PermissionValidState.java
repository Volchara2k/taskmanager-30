package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for permissionValidState.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="permissionValidState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SUCCESS"/&gt;
 *     &lt;enumeration value="LOGGED"/&gt;
 *     &lt;enumeration value="NEED_LOG_IN"/&gt;
 *     &lt;enumeration value="NO_ACCESS_RIGHTS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "permissionValidState")
@XmlEnum
public enum PermissionValidState {

    SUCCESS,
    LOGGED,
    NEED_LOG_IN,
    NO_ACCESS_RIGHTS;

    public static PermissionValidState fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
