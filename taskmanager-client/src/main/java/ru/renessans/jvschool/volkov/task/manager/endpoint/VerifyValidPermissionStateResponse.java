
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyValidPermissionStateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="verifyValidPermissionStateResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="permissionValidState" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}permissionValidState" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyValidPermissionStateResponse", propOrder = {
    "permissionValidState"
})
public class VerifyValidPermissionStateResponse {

    @XmlSchemaType(name = "string")
    protected PermissionValidState permissionValidState;

    /**
     * Gets the value of the permissionValidState property.
     * 
     * @return
     *     possible object is
     *     {@link PermissionValidState }
     *     
     */
    public PermissionValidState getPermissionValidState() {
        return permissionValidState;
    }

    /**
     * Sets the value of the permissionValidState property.
     * 
     * @param value
     *     allowed object is
     *     {@link PermissionValidState }
     *     
     */
    public void setPermissionValidState(PermissionValidState value) {
        this.permissionValidState = value;
    }

}
