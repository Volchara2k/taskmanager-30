package ru.renessans.jvschool.volkov.task.manager.command.admin.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    @NotNull
    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    @NotNull
    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        final boolean removeState = dataEndpoint.dataBinClear(current);
        ViewUtil.print(NOTIFY_BIN_CLEAR);
        ViewUtil.print(removeState);
    }

}