package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for openSessionResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="openSessionResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="openedSession" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}sessionDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "openSessionResponse", propOrder = {
        "openedSession"
})
public class OpenSessionResponse {

    protected SessionDTO openedSession;

    /**
     * Gets the value of the openedSession property.
     *
     * @return possible object is
     * {@link SessionDTO }
     */
    public SessionDTO getOpenedSession() {
        return openedSession;
    }

    /**
     * Sets the value of the openedSession property.
     *
     * @param value allowed object is
     *              {@link SessionDTO }
     */
    public void setOpenedSession(SessionDTO value) {
        this.openedSession = value;
    }

}
