package ru.renessans.jvschool.volkov.task.manager.command.admin.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataBinaryImportCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_BIN_IMPORT = "data-bin-import";

    @NotNull
    private static final String DESC_BIN_IMPORT = "импортировать домен из бинарного вида";

    @NotNull
    private static final String NOTIFY_BIN_IMPORT =
            "Происходит попытка инициализации процесса загрузки домена из бинарного вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_IMPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        @NotNull final DomainDTO domain = dataEndpoint.importDataBin(current);
        ViewUtil.print(NOTIFY_BIN_IMPORT);
        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}