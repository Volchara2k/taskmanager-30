package ru.renessans.jvschool.volkov.task.manager.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class UserSignUpCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_SIGN_UP = "sign-up";

    @NotNull
    private static final String DESC_SIGN_UP = "зарегистрироваться в системе";

    @NotNull
    private static final String NOTIFY_SIGN_UP = "Происходит попытка инициализации регистрации пользователя. \n" +
            "Для регистрации пользователя в системе введите логин и пароль: ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_SIGN_UP;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_SIGN_UP;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_SIGN_UP);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AuthenticationEndpoint authenticationEndpoint = endpointLocator.getAuthenticationEndpoint();

        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();

        @Nullable final SessionDTO current = currentSessionService.getSession();
        @Nullable final UserLimitedDTO user = authenticationEndpoint.signUpUser(current, login, password);
        ViewUtil.print(user);
    }

}