package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.command.EmptyCommandException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.EmptyLocatorException;

import java.util.Collection;
import java.util.Objects;

@AllArgsConstructor
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @NotNull
    @SneakyThrows
    @Override
    public Collection<AbstractCommand> createCommands(
            @Nullable final IApplicationContextService applicationContext
    ) {
        if (Objects.isNull(applicationContext)) throw new EmptyLocatorException();
        @NotNull final Collection<AbstractCommand> commands = this.commandRepository.getAllCommands();
        commands.forEach(command -> command.setApplicationContext(applicationContext));
        return commands;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return this.commandRepository.getAllCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return this.commandRepository.getAllTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return this.commandRepository.getAllArgumentCommands();
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getTerminalCommand(
            @Nullable final String commandLine
    ) {
        if (Objects.isNull(commandLine)) throw new EmptyCommandException();
        return this.commandRepository.getTerminalCommand(commandLine);
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getArgumentCommand(
            @Nullable final String commandLine
    ) {
        if (Objects.isNull(commandLine)) throw new EmptyCommandException();
        return this.commandRepository.getArgumentCommand(commandLine);
    }

}