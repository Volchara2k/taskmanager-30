package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskViewByIndexCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";

    @NotNull
    private static final String DESC_TASK_VIEW_BY_INDEX = "просмотреть задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_VIEW_BY_INDEX =
            "Происходит попытка инициализации отображения задачи. \n" +
                    "Для отображения задачи по индексу введите индекс задачи из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_VIEW_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final TaskDTO task = taskEndpoint.getTaskByIndex(current, index);
        ViewUtil.print(task);
    }

}