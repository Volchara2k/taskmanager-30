package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataBase64ClearCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_BASE64_CLEAR = "data-base64-clear";

    @NotNull
    private static final String DESC_BASE64_CLEAR = "очистить base64 данные";

    @NotNull
    private static final String NOTIFY_BASE64_CLEAR = "Происходит процесс очищения base64 данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BASE64_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceContextService serviceLocator = super.applicationContext.getServiceContext();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointContextService endpointLocator = super.applicationContext.getEndpointContext();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        final boolean removeState = dataEndpoint.dataBase64Clear(current);
        ViewUtil.print(NOTIFY_BASE64_CLEAR);
        ViewUtil.print(removeState);
    }

}