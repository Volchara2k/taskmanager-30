package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;

public interface ICurrentSessionService {

    @Nullable
    SessionDTO getSession();

    @NotNull
    SessionDTO subscribe(@Nullable SessionDTO session);

    @NotNull
    SessionDTO unsubscribe();

}