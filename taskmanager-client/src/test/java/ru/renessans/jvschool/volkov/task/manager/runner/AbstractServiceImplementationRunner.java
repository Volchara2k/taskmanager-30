package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.service.CurrentSessionServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.LocatorServiceTest;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorServiceTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(ServiceImplementation.class)
@Suite.SuiteClasses(
        {
                CurrentSessionServiceTest.class,
                LocatorServiceTest.class,
                ServiceLocatorServiceTest.class
        }
)

public abstract class AbstractServiceImplementationRunner {
}