package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.IApplicationContextService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceContextService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.EmptyLocatorException;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CommandRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceContextRepository;

import java.util.Collection;

@Category(IntegrationImplementation.class)
public final class CommandServiceTest {

    @NotNull
    private final IServiceContextRepository serviceLocatorRepository = new ServiceContextRepository();

    @NotNull
    private final IServiceContextService serviceLocator = new ServiceContextService(serviceLocatorRepository);

    @NotNull
    private final IEndpointContextRepository endpointLocatorRepository = new EndpointContextRepository();

    @NotNull
    private final IEndpointContextService endpointLocator = new EndpointContextService(endpointLocatorRepository);

    @NotNull
    private final IApplicationContextService locatorService = new ApplicationContextService(
            endpointLocator, serviceLocator
    );

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Test(expected = EmptyLocatorException.class)
    @TestCaseName("Run testNegativeInitialCommands for initialCommands(null)")
    public void testNegativeInitialCommands() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        this.commandService.createCommands(null);
    }

    @Test
    @TestCaseName("Run testInitialCommands for initialCommands(locator)")
    public void testInitialCommands() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        Assert.assertNotNull(this.locatorService);
        @NotNull final Collection<AbstractCommand> initialCommands = this.commandService.createCommands(locatorService);
        Assert.assertNotNull(initialCommands);
        Assert.assertNotEquals(0, initialCommands.size());
    }

    @Test
    @TestCaseName("Run testGetAllCommands for getAllCommands()")
    public void testGetAllCommands() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allCommands = this.commandService.getAllCommands();
        Assert.assertNotNull(allCommands);
        Assert.assertNotEquals(0, allCommands.size());
    }

    @Test
    @TestCaseName("Run testGetAllTerminalCommands for getAllTerminalCommands()")
    public void testGetAllTerminalCommands() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allTerminalCommands = this.commandService.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
        Assert.assertNotEquals(0, allTerminalCommands.size());
    }

    @Test
    @TestCaseName("Run testAllArgumentCommands for getAllArgumentCommands()")
    public void testAllArgumentCommands() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allArgumentCommands = this.commandService.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
        Assert.assertNotEquals(0, allArgumentCommands.size());
    }

    @Test
    @TestCaseName("Run testGetTerminalCommand for getTerminalCommand(command)")
    public void testGetTerminalCommand() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        @NotNull final String command = "help";
        Assert.assertNotNull(command);
        @Nullable final AbstractCommand terminalCommand = this.commandService.getTerminalCommand(command);
        Assert.assertNotNull(terminalCommand);
    }

    @Test
    @TestCaseName("Run testGetArgumentCommand for getArgumentCommand(argument)")
    public void testGetArgumentCommand() {
        Assert.assertNotNull(this.commandRepository);
        Assert.assertNotNull(this.commandService);
        @NotNull final String argument = "-h";
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand command = this.commandService.getArgumentCommand(argument);
        Assert.assertNotNull(command);
    }

}