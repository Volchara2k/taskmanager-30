package ru.renessans.jvschool.volkov.task.manager.casedata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class CaseDataValidRuleProvider {

    @SuppressWarnings("unused")
    public Object[] invalidCollectionsCaseData() {
        return new Object[]{
                new Object[]{true, null},
                new Object[]{
                        true,
                        Arrays.asList(
                                null,
                                null,
                                null
                        )
                },
                new Object[]{true, Collections.emptyList()},
                new Object[]{true, new ArrayList<>()}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validStringsCaseData() {
        return new Object[]{
                new Object[]{true, "null"},
                new Object[]{true, "    1"},
                new Object[]{true, "dwa       dwa"},
                new Object[]{true, "1      "},
                new Object[]{true, "#!@  "}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validCollectionsCaseData() {
        return new Object[]{
                new Object[]{
                        false,
                        Arrays.asList(
                                "",
                                "",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "null",
                                "not null",
                                ""
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                null,
                                "wolf",
                                null
                        )
                },
                new Object[]{
                        false,
                        Arrays.asList(
                                "string",
                                "data    ",
                                ", c..ca"
                        )
                }
        };
    }

}