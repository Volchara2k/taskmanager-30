package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointContextService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointContextRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointContextService;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AuthenticationEndpointTest {

    @NotNull
    private final IEndpointContextRepository endpointLocatorRepository = new EndpointContextRepository();

    @NotNull
    private final IEndpointContextService endpointLocator = new EndpointContextService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = endpointLocator.getAuthenticationEndpoint();

    @Test
    @TestCaseName("Run testSignUpUser for signUpUser(null, random, random)")
    public void testSignUpUser() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(addUser.getId(), addUser.getId());
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final SessionDTO open = sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final UserLimitedDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

}